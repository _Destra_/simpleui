////////////////////////////////////////////////////////////////////////////////
// Copyright Antonin "Destra" Gagelin  
////////////////////////////////////////////////////////////////////////////////

package com.destra.simpleui.interfaces
{
	
	/**
	 * @author Antonin 'Destra' Gagelin (destravul[AT]gmail[DOT]com)
	 * Oct 16, 2013
	 */
	public interface IDisposable
	{
		/**
		 * Is this object has been disposed ?
		 */
		function get isDisposed() : Boolean;
		
		/**
		 * Dispose the object in order to be garbage collected.
		 */
		function dispose() : void;
	}
}
